#!/usr/bin/python
# https://github.com/ema/pycodicefiscale
# http://pymysql.readthedocs.io/en/latest/index.html

import os
import sys
import csv
import codecs 
from pprint import pprint
import datetime
from codicefiscale import *
# from xml.etree.ElementTree import Element, SubElement, Comment
import xml.etree.ElementTree as ET
import xml.dom.minidom
import pymysql.cursors
from decimal import Decimal


# Connect to the database
connection = pymysql.connect(host='localhost', user='stefano', password='password', db='valcanale', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

# create the file structure
ns3 = ET.Element('ns3:DatiFattura')
ns3.set('xmlns:ns2', 'http://www.w3.org/2000/09/xmldsig#')
ns3.set('xmlns:ns3', 'http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v2.0')
ns3.set('versione', 'DAT20')
data = ET.SubElement(ns3, 'DTE')
items = ET.SubElement(data, 'CedentePrestatoreDTE')
item1 = ET.SubElement(items, 'IdentificativiFiscali')
item2 = ET.SubElement(item1, 'IdFiscaleIVA')
item3 = ET.SubElement(item2, 'IdPaese')
item4 = ET.SubElement(item2, 'IdCodice')
item5 = ET.SubElement(item1, 'CodiceFiscale')
item6 = ET.SubElement(items, 'AltriDatiIdentificativi')
item7 = ET.SubElement(item6, 'Denominazione')
# item8 = ET.SubElement(item6, 'Nome')
# item9 = ET.SubElement(item6, 'Cognome')
item10 = ET.SubElement(item6, 'Sede')
item11 = ET.SubElement(item10, 'Indirizzo')
item12 = ET.SubElement(item10, 'CAP')
item13 = ET.SubElement(item10, 'Comune')
item14 = ET.SubElement(item10, 'Provincia')
item15 = ET.SubElement(item10, 'Nazione')

# item1.set('name','item1')
# item2.set('name','item2')
item3.text = 'IT'
item4.text = '02756330300'
item5.text = '02756330300'
item7.text = 'Valcanale Energia S.r.l'
item11.text = 'VIA OFFICINE, 10'
item12.text = '33018'
item13.text = 'Tarvisio'
item14.text = 'UD'
item15.text = 'IT'

# create a new XML file with the results
mydata = ET.tostring(ns3)  
myfile = open("final_october.xml", "w")  
myfile.write(mydata)

try:
	with connection.cursor() as cursor:
		sql = "SELECT * FROM `invoices` "
		cursor.execute(sql)
		result = cursor.fetchall()
		for row in result:
			# spesometro 2017 (da Luglio a Dicembre 2017):
			if ("2018" not in str(row['invoice_date'])) and (str(row['invoice_date']).split('-')[1] >= "07"):
				#spesometro 2018 (da Gennaio a Giugno 2018) spostare di un tab! if ("2017" not in str(row['invoice_date'])):
				item16 = ET.SubElement(data, 'CessionarioCommittenteDTE')
				item17 = ET.SubElement(item16, 'IdentificativiFiscali')
				# print(str(row['invoice_id']))
				
				if (str(row['customer_vat']) == "-" or str(row['customer_vat']) == ""):
					stub100 = ET.SubElement(item17, 'CodiceFiscale')
					stub100.text = str(row['customer_cf'])
				else: 
					stub99 = ET.SubElement(item17, 'IdFiscaleIVA')
					if (str(row['customer_vat']) == str(row['customer_cf'])):
						stub100 = ET.SubElement(item17, 'CodiceFiscale')
						stub100.text = str(row['customer_vat'])
					else:
						stub100 = ET.SubElement(item17, 'CodiceFiscale')
						stub100.text = str(row['customer_cf'])
					stub102 = ET.SubElement(stub99, 'IdPaese')
					stub102.text = str("IT")
					stub101 = ET.SubElement(stub99, 'IdCodice')
					stub101.text = str(row['customer_vat'])
				# 
				item30 = ET.SubElement(item16, 'DatiFatturaBodyDTE')
				item31 = ET.SubElement(item30, 'DatiGenerali')
				item32 = ET.SubElement(item31, 'TipoDocumento') # da scrivere
				item32.text = str("TD01")
				item33 = ET.SubElement(item31, 'Data') # da scrivere, invoice_date
				item33.text = str(row['invoice_date'])
				item34 = ET.SubElement(item31, 'Numero') # da scrivere, invoice_id
				item34.text = str(row['invoice_id'])
				print(str(row['invoice_id']))
				item35 = ET.SubElement(item30, 'DatiRiepilogo')
				item36 = ET.SubElement(item35, 'ImponibileImporto')
				item36.text = str("%.2f" % row['price_exl_vat']) # da scrivere, price_with_vat
				item37 = ET.SubElement(item35, 'DatiIVA') 
				item38 = ET.SubElement(item37, 'Imposta') # da scrivere, price_vat
				item38.text = str("%.2f" % row['price_vat'])
				item39 = ET.SubElement(item37, 'Aliquota') # da scrivere
				item39.text = str("%.2f" % 22)
finally:
	connection.close()
mydata = ET.tostring(ns3)  
myfile = open("final_october.xml", "w")  
myfile.write(mydata)
myfile.close()

