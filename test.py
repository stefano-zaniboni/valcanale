#!/usr/bin/python
# https://github.com/ema/pycodicefiscale
# http://pymysql.readthedocs.io/en/latest/index.html

import os
import sys
import csv
import codecs 
from pprint import pprint
import datetime
from codicefiscale import *
# from xml.etree.ElementTree import Element, SubElement, Comment
import xml.etree.ElementTree as ET
import xml.dom.minidom
import pymysql.cursors

def change(x):
    	return{
			'MRARSO74P45F356W':'0,Rosamaria,Anderwald',
			'DLERNZ69D19Z133V':'1,Delbianco',
			'SRNSFN60T14Z129Y':'0,SorinFlorin,Stefanuta',
			'NTNMHL53L03A893F':'0,Michele,Trovato',
			'DIXGLI54D27A692A':'1,DiFlaviano',
			'CRSMRA59H62I809W':'0,MariaCristina,infante',
			'DEXTLL56A10G309W':'1,Derosa',
			'GHRSFN86T15Z129T':'0,Gheorghita,Stefan',
			'DLESFN67M70L195W':'1,Delfabbro',
			'DEXCST83P17F839E':'1,Derosa',
			'NTNFMN55S57G942B':'0,Filomena,Camardese',
			'MRAMDA59A21F963P':'0,MauroAmedeo,Franchin',
			'DEXGNN57R11L424F':'1,Devisentini',
			'DIXCLD50D59L424W':'1,Digiorgio',
			'MRANNA67E70L057M':'0,AnnaMaria,Tschek',
			'DLELSI91D63D962F':'1,Delnegro',
			'DEXNDR95T12D962Y':'1,Derosa',
			'DIXLNA92D13L195M':'1,Divora',
			'PLADMN66L18D962Z':'0,DamianoPaolo,Matiz',
			'DEXLGU59A15G309X':'1,Derosa',
			'LNEMRA75H56F205Y':'0,Mariaelena,Turcinovich',
			'DLELRD73C24D962Q':'1,Delnegro',
			'MRARSO65H54A944I':'0,Rosamaria,Palmieri',
			'DIXLSS82T13D962D':'1,Direnzo',
			'DEXMRA57S10G198U':'1,Dicaneva',
			'DEXWTR55S23L057G':'1,Dereggi',
			'DIXLSN68P19L483D':'1,Difelice',
			'GSPGLI52R26L859U':'0,GiulioGiuseppe,Petrullo',
			'DXXDVD82H17F537J':'1,dangelo',
			'DEXLDN61R47L381I':'1,decillia',
			'MRCRLF54D21L057W':'0,Rodolfo,Kravina',
			'GRGPRI70R05H816C':'0,Piergiorgio,Domini',
			'JHNDNL70S05Z719G':'0,DanielJohn,Roberts'
		}[x]

def hardExceptions(x):
	return{
	'SKRGCR18D43L057O':'Giancarlo,Skert',
	'VNCGNR86R16G273P':'Vincenzo,Gennaro',
	'TRBCST53T48E846D':'Cristina,Terlicher', # Cristina Tributsch coniugata Terlicher
	'FLCGLN86P18G642A':'Felice,Giugliano',
	'RMNGDU54P15G284F':'Romano,Guida'
	}[x]

def checker(codice, builded):
	if (codice == builded):
		return True
	else:
		return False

def letters(input):
    return ''.join([c for c in input if c.isalpha()])

# Connect to the database
connection = pymysql.connect(host='localhost',
                             user='stefano',
                             password='password',
                             db='valcanale',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
# Connect to the database
# connection = pymysql.connect(host='localhost',
#                              user='root',
#                              password='root',
#                              db='valcanale',
#                              charset='utf8mb4',
#                              cursorclass=pymysql.cursors.DictCursor)


nomi = []
codici = []
city = []
CAP = []
address = []
file = codecs.open("client_list.csv", "rU", "utf-8")

# create the file structure
ns3 = ET.Element('ns3:DatiFattura')
ns3.set('xmlns:ns2', 'http://www.w3.org/2000/09/xmldsig#')
ns3.set('xmlns:ns3', 'http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v2.0')
ns3.set('versione', 'DAT20')
data = ET.SubElement(ns3, 'DTE')
items = ET.SubElement(data, 'CedentePrestatoreDTE')
item1 = ET.SubElement(items, 'IdentificativiFiscali')
item2 = ET.SubElement(item1, 'IdFiscaleIVA')
item3 = ET.SubElement(item2, 'IdPaese')
item4 = ET.SubElement(item2, 'IdCodice')
item5 = ET.SubElement(item1, 'CodiceFiscale')
item6 = ET.SubElement(items, 'AltriDatiIdentificativi')
item7 = ET.SubElement(item6, 'Denominazione')
item8 = ET.SubElement(item6, 'Nome')
item9 = ET.SubElement(item6, 'Cognome')
item10 = ET.SubElement(item6, 'Sede')
item11 = ET.SubElement(item10, 'Indirizzo')
item12 = ET.SubElement(item10, 'CAP')
item13 = ET.SubElement(item10, 'Comune')
item14 = ET.SubElement(item10, 'Provincia')
item15 = ET.SubElement(item10, 'Nazione')

# item1.set('name','item1')
# item2.set('name','item2')
item3.text = 'IT'
item4.text = '02756330300'
item5.text = '02756330300'
item7.text = 'Valcanale Energia S.r.l'
item11.text = 'VIA OFFICINE, 10'
item12.text = '33018'
item13.text = 'Tarvisio'
item14.text = 'UD'
item15.text = 'IT'

# create a new XML file with the results
mydata = ET.tostring(ns3)  
myfile = open("items2.xml", "w")  
myfile.write(mydata)

reader = csv.reader(file)

for row in reader:
	# print(row[1])
	nomi.append(row[0])
	codici.append(row[1])
	address.append(row[6])
	city.append(row[7])
	CAP.append(row[8])

# print(str(len(nomi)))
# pprint(nomi)

# build('zaniboni', 'stefano', datetime.datetime(1994, 10, 31), 'M', 'd548')
# the head of the stack is useless, pop and start
nomi.pop(0)
codici.pop(0)
city.pop(0)
CAP.pop(0)
address.pop(0)
# for nome in nomi:
for nome, codice, user_city, cap, user_addr in zip(nomi, codici, city, CAP, address):
	name = str(nome.split(" ")[0])
	surname = str(nome.split(" ")[1])
	if not (isvalid(codice)):
		if(len(codice) == 11):
			# print("Associazione di qualcosa: " + codice)
			# try:
			#     # connection is not autocommit by default. So you must commit to save
			#     # your changes.
			#     # connection.commit()

			#     with connection.cursor() as cursor:
			#         # Read a single record
			#         sql = "SELECT * FROM `invoices` WHERE `customer_cf`=%s"
			#         cursor.execute(sql, (str(codice),))
			#         result = cursor.fetchall()
			#         # for row in result:
			#         # 	print(str(row['invoice_id']))
			#         # print(str((result[0]['invoice_id'])))
			#         # print(str((result[0]['invoice_id'])))
			item16 = ET.SubElement(data, 'CessionarioCommittenteDTE')
			item17 = ET.SubElement(item16, 'IdentificativiFiscali')
			item18 = ET.SubElement(item17, 'IdFiscaleIVA')
			stub1 = ET.SubElement(item18, 'IdPaese')
			stub1.text = str("IT")
			item18a = ET.SubElement(item18, 'IdCodice')
			item18a.text = str(codice)
			item18b = ET.SubElement(item17, 'CodiceFiscale')
			item18b.text = str(codice)
			item19 = ET.SubElement(item16, 'AltriDatiIdentificativi')
			item20 = ET.SubElement(item19, 'Denominazione')
			item20.text = str("Associazione Culturale della Valcanale ") # inserito a mano
			item21 = ET.SubElement(item19, 'Nome')
			item22 = ET.SubElement(item19, 'Cognome')
			item23 = ET.SubElement(item19, 'Sede')
			item24 = ET.SubElement(item23, 'Indirizzo') # da scrivere
			item24.text = str("Via Bamberga")
			item25 = ET.SubElement(item23, 'NumeroCivico') # da scrivere
			item25.text = str("1")
			item26 = ET.SubElement(item23, 'CAP') # da scrivere
			item26.text = str("33018")
			item27 = ET.SubElement(item23, 'Comune') # da scrivere
			item27.text = str("Tarvisio")
			item28 = ET.SubElement(item23, 'Provincia') # da scrivere
			item28.text = str("UD")
			item29 = ET.SubElement(item23, 'Nazione') # da scrivere
			item29.text = str("IT")
			# SELECT * FROM `invoices` WHERE `customer_cf`="84008720306"
			try:
				with connection.cursor() as cursor:
					sql = "SELECT * FROM `invoices` WHERE `customer_cf`=%s"
					cursor.execute(sql, (str("84008720306"),))
					result = cursor.fetchall()
					for row in result:
						# print(str(row['invoice_id']) + " | " + str(row['price_with_vat']) + " | " + str(row['price_vat']) + " | " + str(row['invoice_date']))
						if ("2018" not in str(row['invoice_date'])) and (str(row['invoice_date']).split("-")[1] >= "07"):
							item30 = ET.SubElement(item16, 'DatiFatturaBodyDTE')
							item31 = ET.SubElement(item30, 'DatiGenerali')
							item32 = ET.SubElement(item31, 'TipoDocumento') # da scrivere
							item32.text = str("TD01")
							item33 = ET.SubElement(item31, 'Data') # da scrivere, invoice_date
							item33.text = str(row['invoice_date'])
							item34 = ET.SubElement(item31, 'Numero') # da scrivere, invoice_id
							item34.text = str(row['invoice_id'])
							print(str(row['invoice_id']))
							item35 = ET.SubElement(item30, 'DatiRiepilogo')
							item36 = ET.SubElement(item35, 'ImponibileImporto')
							item36.text = str(row['price_with_vat']) # da scrivere, price_with_vat
							item37 = ET.SubElement(item35, 'DatiIVA') 
							item38 = ET.SubElement(item37, 'Imposta') # da scrivere, price_vat
							item38.text = str(row['price_vat'])
							item39 = ET.SubElement(item37, 'Aliquota') # da scrivere
							item39.text = str(22.00)
			finally:
				# do something
				pass

	else:
		born = get_birthday(codice).split("-")
		builded = build(surname, name, datetime.datetime(int("19" + born[2]), int(born[1]), int(born[0])), get_sex(codice), codice[-5:-1])
		if not (checker(codice, builded)):
			# Probably Exception: people that have 2 names or wrong Codice Fiscale. Let's check if the build is correct
			if (isvalid(codice)):
				# Maybe Error in builded, divide people with Name/Surname(hard exceptions) and people that have more name/surnames
				if (len(nome.split(" ")) == 2):
					new = hardExceptions(str(builded))
					name = new.split(",")[0]
					surname = new.split(",")[1]
					# pass
				else:			
					# name = str(nome.split(" ")[0] + nome.split(" ")[1])
					# surname = str(nome.split(" ")[2])
					# print(str(len(nome.split(" "))) + " | " + nome + " | " + codice + " | " + builded)
					new = change(str(builded))
					if (new.split(',')[0] == "0"):
						name = new.split(',')[1]
						surname = new.split(',')[2]
						# print(name)
					else:
						surname = new.split(',')[1]
					# i've tested and i solved only the case of 2 surnames
			builded = build(surname, name, datetime.datetime(int("19" + born[2]), int(born[1]), int(born[0])), get_sex(codice), codice[-5:-1])
			if not (checker(codice, builded)):
				# print(str(len(nome.split(" "))) + " | " + nome + " || "+ surname + " | "+ name + " | " + codice + " | " + builded + " | " + str(isvalid(builded)))
				pass
		else:
			# Corretti
			#print(name + " " + surname + " " + codice + " " + get_birthday(codice) + " " + get_sex(codice) + " " + codice[-5:-1] + " " + builded + " " + str(checker(codice, builded)))
			#print(str(checker(codice, builded)))
			pass
		# print(str(builded) + " | " + surname)
		item16 = ET.SubElement(data, 'CessionarioCommittenteDTE')
		item17 = ET.SubElement(item16, 'IdentificativiFiscali')
		item18 = ET.SubElement(item17, 'CodiceFiscale')
		item18.text = str(builded)
		item19 = ET.SubElement(item16, 'AltriDatiIdentificativi')
		item20 = ET.SubElement(item19, 'Denominazione')
		item20.text = str(name) + " " + str(surname)
		item21 = ET.SubElement(item19, 'Nome')
		item21.text = str(name)
		item22 = ET.SubElement(item19, 'Cognome')
		item22.text = str(surname)
		item23 = ET.SubElement(item19, 'Sede')
		item24 = ET.SubElement(item23, 'Indirizzo') # da scrivere
		item24.text = str(letters(user_addr))
		item25 = ET.SubElement(item23, 'NumeroCivico') # da scrivere
		item25.text = str(filter(str.isdigit, user_addr))
		item26 = ET.SubElement(item23, 'CAP') # da scrivere
		item26.text = str(cap)
		item27 = ET.SubElement(item23, 'Comune') # da scrivere
		item27.text = str(user_city)
		item28 = ET.SubElement(item23, 'Provincia') # da scrivere
		item28.text = str("UD")
		item29 = ET.SubElement(item23, 'Nazione') # da scrivere
		item29.text = str("IT")
		try:
			#
			#
			#
			with connection.cursor() as cursor:
				# 
				sql = "SELECT `invoice_id`, `customer_name`, `price_vat`, `price_with_vat`, `invoice_date`, `customer_code` FROM `invoices` WHERE `customer_cf`=%s"
				cursor.execute(sql, (str(builded),))
				result = cursor.fetchall()
				for row in result:
					# print(str(row['invoice_id']) + " | " + str(row['price_with_vat']) + " | " + str(row['price_vat']) + " | " + str(row['invoice_date']))
					if ("2018" not in str(row['invoice_date'])) and (str(row['invoice_date']).split("-")[1] >= "07") and (str(row['customer_code'] == "-")):
						item30 = ET.SubElement(item16, 'DatiFatturaBodyDTE')
						item31 = ET.SubElement(item30, 'DatiGenerali')
						item32 = ET.SubElement(item31, 'TipoDocumento') # da scrivere
						item32.text = str("TD01")
						item33 = ET.SubElement(item31, 'Data') # da scrivere, invoice_date
						item33.text = str(row['invoice_date'])
						item34 = ET.SubElement(item31, 'Numero') # da scrivere, invoice_id
						item34.text = str(row['invoice_id'])
						print(str(row['invoice_id']))
						item35 = ET.SubElement(item30, 'DatiRiepilogo')
						item36 = ET.SubElement(item35, 'ImponibileImporto')
						item36.text = str(row['price_with_vat']) # da scrivere, price_with_vat
						item37 = ET.SubElement(item35, 'DatiIVA') 
						item38 = ET.SubElement(item37, 'Imposta') # da scrivere, price_vat
						item38.text = str(row['price_vat'])
						item39 = ET.SubElement(item37, 'Aliquota') # da scrivere
						item39.text = str(22.00)
		finally:
			# do something
			pass



################################ New section for business ################################ 

# Main query from db for business tenants: SELECT * FROM `invoices` WHERE NOT `customer_vat`="-"

try:
    # connection is not autocommit by default. So you must commit to save
    # your changes.
    # connection.commit()

    with connection.cursor() as cursor:
        # Read a single record
        # SELECT DISTINCT `customer_code`, `customer_name` FROM `invoices` WHERE NOT `customer_vat`="-"
        sql = "SELECT DISTINCT `customer_code`, `customer_name`, `customer_vat`, `customer_cf` FROM `invoices` WHERE NOT `customer_vat`=%s"
        cursor.execute(sql, (str("-"),))
        result = cursor.fetchall()
        for row in result:
        	item16 = ET.SubElement(data, 'CessionarioCommittenteDTE')
        	item17 = ET.SubElement(item16, 'IdentificativiFiscali')
        	stub100 = ET.SubElement(item17, 'CodiceFiscale')
        	if (str(row['customer_vat']) == str(row['customer_cf'])):
        		stub100.text = str(row['customer_vat'])
        	else:
        		stub100.text = str(row['customer_cf'])
        	stub99 = ET.SubElement(item17, 'IdFiscaleIVA')
        	stub101 = ET.SubElement(stub99, 'IdCodice')
        	stub101.text = str(row['customer_vat'])
        	stub102 = ET.SubElement(stub99, 'IdPaese')
        	stub102.text = str("IT")
        	try: 
        		#
        		#
        		#
        		with connection.cursor() as cursor:
        			# 
        			sql = "SELECT * FROM `invoices` WHERE `customer_code`=%s"
        			cursor.execute(sql, (str(row['customer_code']),))
        			result = cursor.fetchall()
        			for row in result:
        				# print(str(row['invoice_id']) + " | " + str(row['price_with_vat']) + " | " + str(row['price_vat']) + " | " + str(row['invoice_date']))
        				if ("2018" not in str(row['invoice_date'])) and (str(row['invoice_date']).split("-")[1] >= "07"):
        					item30 = ET.SubElement(item16, 'DatiFatturaBodyDTE')
        					item31 = ET.SubElement(item30, 'DatiGenerali')
        					item32 = ET.SubElement(item31, 'TipoDocumento') # da scrivere
        					item32.text = str("TD01")
        					item33 = ET.SubElement(item31, 'Data') # da scrivere, invoice_date
        					item33.text = str(row['invoice_date'])
        					item34 = ET.SubElement(item31, 'Numero') # da scrivere, invoice_id
        					item34.text = str(row['invoice_id'])
        					print(str(row['invoice_id']))
        					item35 = ET.SubElement(item30, 'DatiRiepilogo')
        					item36 = ET.SubElement(item35, 'ImponibileImporto')
        					item36.text = str(row['price_with_vat']) # da scrivere, price_with_vat
        					item37 = ET.SubElement(item35, 'DatiIVA') 
        					item38 = ET.SubElement(item37, 'Imposta') # da scrivere, price_vat
        					item38.text = str(row['price_vat'])
        					item39 = ET.SubElement(item37, 'Aliquota') # da scrivere
        					item39.text = str(22.00)
		finally:
			# do something
			pass
        # print(str((result[0]['invoice_id'])))
        # print(str((result[0]['invoice_id'])))
        # visto il nuovo xml non serve l'anagrafica, mi prendo i codici fiscali e le partite iva, creo la parte relativa alla fatturazione end
finally:
    connection.close()

mydata = ET.tostring(ns3)  
myfile = open("items2.xml", "w")  
myfile.write(mydata)
file.close()
myfile.close()









